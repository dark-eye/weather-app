function getMoonPhaseString(forecastDateStart,forecastDateEnd) {
    //moonphase returned as value between 0 and 1,
    //date.date is set to 11:59 am of the day concerned
    //calculate the moonphasevalues for start of day (= 0:00) and end of day (= 23:59) by adding/subtracting 12 hours
    //compare start and end values against moonphasevalues to set the name of the moonphase
    var dayStart = SunCalc.SunCalc.getMoonIllumination(forecastDateStart).phase;
    var dayEnd = SunCalc.SunCalc.getMoonIllumination(forecastDateEnd).phase;
    var moonPhaseString = "";
    var moonEmoji ="";

    if (dayStart > dayEnd) {
        moonPhaseString = i18n.tr("New moon");
        moonEmoji = "\uD83C\uDF11";
    } else if (dayStart > 0.00 && dayEnd < 0.25) {
        moonPhaseString = i18n.tr("Waxing Crescent");
        moonEmoji = "\uD83C\uDF12";
    } else if (dayStart <= 0.25 && dayEnd >= 0.25) {
        moonPhaseString = i18n.tr("First Quarter");
        moonEmoji = "\uD83C\uDF13";
    } else if (dayStart > 0.25 && dayEnd < 0.50) {
        moonPhaseString = i18n.tr("Waxing Gibbous");
        moonEmoji = "\uD83C\uDF14";
    } else if (dayStart <= 0.50 && dayEnd >= 0.50) {
        moonPhaseString = i18n.tr("Full moon");
        moonEmoji = "\uD83C\uDF15";
    } else if (dayStart > 0.50 && dayEnd < 0.75) {
        moonPhaseString = i18n.tr("Waning Gibbous");
        moonEmoji = "\uD83C\uDF16";
    } else if (dayStart <= 0.75 && dayEnd >= 0.75) {
        moonPhaseString = i18n.tr("Last Quarter");
        moonEmoji = "\uD83C\uDF17";
    } else if (dayStart > 0.75 && dayEnd < 1.00) {
        moonPhaseString = i18n.tr("Waning Crescent");
        moonEmoji = "\uD83C\uDF18";
    } else {
        moonPhaseString = i18n.tr("calculation error");
    }

    return {
      moonphase: moonPhaseString,
      moonEmoji: moonEmoji
    }
}
