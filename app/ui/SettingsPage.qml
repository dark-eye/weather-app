/*
 * Copyright (C) 2015-2016 Canonical Ltd
 *
 * This file is part of Ubuntu Weather App
 *
 * Ubuntu Weather App is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * Ubuntu Weather App is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Ubuntu.Components 1.3
import "../components"
import "settings"
Page {
    id: settingsPage

    header: PageHeader {
        title: i18n.tr("Settings")
    }

    property bool bug1341671workaround: true

    Flickable {
        id: settingsFlickable
        clip: true

        anchors {
            topMargin: settingsPage.header.height + units.gu(3)
            fill: parent
        }

        contentHeight: settingsColumn.childrenRect.height

        Column {
            id: settingsColumn

            anchors {
                top: parent.top
                left: parent.left
                right: parent.right
            }

            ListItem {
                width: parent.width
                height: units.gu(4)

                Label {
                  id: versionText
                  anchors { left: parent.left; right: parent.right; margins: units.gu(2) }
                  font.weight: Font.DemiBold
                  text: i18n.tr("app version: %1").arg("v4.5.2")
                }
            }

            ListItem {
                width: parent.width
                height: upg.height > 0 ? upg.height : units.gu(7)

                TempUnitsPage {
                    id: upg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: wupg.height > 0 ? wupg.height : units.gu(7)

                WindUnitsPage {
                    id: wupg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: ripg.height > 0 ? ripg.height : units.gu(7)

                RefreshIntervalPage {
                    id: ripg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: lpg.height > 0 ? lpg.height : units.gu(7)

                LocationPage {
                    id: lpg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: tpg.height > 0 ? tpg.height : units.gu(7)

                ThemePage {
                    id: tpg
                    width: parent.width
                }
            }

            ListItem {
                width: parent.width
                height: dpp.height > 0 ? dpp.height : units.gu(7)

                DataProviderPage {
                    id: dpp
                    width: parent.width
                }
            }
        }
    }
}
