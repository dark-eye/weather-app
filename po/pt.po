# Portuguese translation for ubuntu-weather-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-weather-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-weather-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 12:33+0000\n"
"PO-Revision-Date: 2019-05-10 21:03+0000\n"
"Last-Translator: Ivo Xavier <ivofernandes12@gmail.com>\n"
"Language-Team: Portuguese <https://translate.ubports.com/projects/ubports/"
"weather-app/pt/>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.6.1\n"
"X-Launchpad-Export-Date: 2017-04-27 05:56+0000\n"

#: ../app/components/DayDelegateExtraInfo.qml:52
msgid "Chance of precipitation"
msgstr "Possibilidade de chuva"

#: ../app/components/DayDelegateExtraInfo.qml:59
msgid "Winds"
msgstr "Ventos"

#: ../app/components/DayDelegateExtraInfo.qml:68
msgid "UV Index"
msgstr "Índice UV"

#: ../app/components/DayDelegateExtraInfo.qml:74
msgid "Pollen"
msgstr "Pólen"

#: ../app/components/DayDelegateExtraInfo.qml:81
msgid "Humidity"
msgstr "Umidade"

#: ../app/components/DayDelegateExtraInfo.qml:88
msgid "Sunrise"
msgstr "Amanhecer"

#: ../app/components/DayDelegateExtraInfo.qml:95
msgid "Sunset"
msgstr "Pôr do sol"

#: ../app/components/DayDelegateExtraInfo.qml:102
msgid "Moonphase"
msgstr "Fase da lua"

#: ../app/components/HeadState/LocationsHeadState.qml:38
#: ../app/components/HeadState/MultiSelectHeadState.qml:38
#: ../app/ui/HomePage.qml:43
msgid "Locations"
msgstr "Locais"

#: ../app/components/HeadState/MultiSelectHeadState.qml:32
msgid "Cancel selection"
msgstr "Cancelar seleção"

#: ../app/components/HeadState/MultiSelectHeadState.qml:43
msgid "Select All"
msgstr "Selecionar todos"

#: ../app/components/HeadState/MultiSelectHeadState.qml:55
msgid "Delete"
msgstr "Eliminar"

#: ../app/components/HomePageEmptyStateComponent.qml:51
msgid "Searching for current location..."
msgstr "A procurar localização atual..."

#: ../app/components/HomePageEmptyStateComponent.qml:52
msgid "Cannot determine your location"
msgstr "Não foi possível determinar a sua localização"

#: ../app/components/HomePageEmptyStateComponent.qml:62
msgid "Manually add a location by swiping up from the bottom of the display"
msgstr "Adicionar manualmente um local ao deslizar do fundo do ecrã"

#: ../app/components/HomeTempInfo.qml:108
msgid "Today"
msgstr "Hoje"

#: ../app/components/HomeTempInfo.qml:112
#, qt-format
msgid "updated %1 day ago"
msgid_plural "updated %1 days ago"
msgstr[0] "atualizado há %1 dia"
msgstr[1] "atualizado há %1 dias"

#: ../app/components/HomeTempInfo.qml:114
#, qt-format
msgid "updated %1 hour ago"
msgid_plural "updated %1 hours ago"
msgstr[0] "atualizado há %1 hora"
msgstr[1] "atualizado há %1 horas"

#: ../app/components/HomeTempInfo.qml:116
#, qt-format
msgid "updated %1 minute ago"
msgid_plural "updated %1 minutes ago"
msgstr[0] "atualizado há %1 minuto"
msgstr[1] "atualizado há %1 minutos"

#: ../app/components/HomeTempInfo.qml:118
msgid "updated recently"
msgstr "atualizado recentemente"

#: ../app/components/ListItemActions/Remove.qml:26
msgid "Remove"
msgstr "Remover"

#: ../app/components/LocationsPageEmptyStateComponent.qml:36
msgid "No locations found. Tap the plus icon to search for one."
msgstr "Sem locais encontrados. Clique no botão '+' para procurar."

#: ../app/components/NetworkErrorStateComponent.qml:47
msgid "Network Error"
msgstr "Erro de rede"

#: ../app/components/NetworkErrorStateComponent.qml:56
msgid ""
"No network connection. Could not load weather data.\n"
"Please check your network settings and try again."
msgstr ""
"Sem ligação de rede. Impossível carregar dados meteorológicos.\n"
"Por favor, verifique as suas definições de rede e tente novamente."

#: ../app/components/NetworkErrorStateComponent.qml:62
msgid "Retry"
msgstr "Tentar novamente"

#: ../app/components/NoAPIKeyErrorStateComponent.qml:47
msgid "No API Keys Found"
msgstr "Sem chaves de API encontradas"

#: ../app/components/NoAPIKeyErrorStateComponent.qml:56
msgid ""
"If you are a developer, please see the README file for instructions on how "
"to obtain your own Open Weather Map API key."
msgstr ""
"Se é um programador, leia o ficheiro README para instruções como obter a sua "
"própria chave de API do Open Weather Map."

#: ../app/data/moonphase.js:12
msgid "New moon"
msgstr "Lua nova"

#: ../app/data/moonphase.js:15
msgid "Waxing Crescent"
msgstr "Quarto crescente"

#: ../app/data/moonphase.js:18
msgid "First Quarter"
msgstr "Primeiro quarto"

#: ../app/data/moonphase.js:21
msgid "Waxing Gibbous"
msgstr "Lua Gibosa"

#: ../app/data/moonphase.js:24
msgid "Full moon"
msgstr "Lua cheia"

#: ../app/data/moonphase.js:27
msgid "Waning Gibbous"
msgstr "Lua Balsâmica"

#: ../app/data/moonphase.js:30
msgid "Last Quarter"
msgstr "Último quarto"

#: ../app/data/moonphase.js:33
msgid "Waning Crescent"
msgstr "Lua Minguante"

#: ../app/data/moonphase.js:36
msgid "calculation error"
msgstr "erro de cálculo"

#: ../app/ubuntu-weather-app.qml:245
msgid ""
"One arguments for weather app: --display, --city, --lat, --lng They will be "
"managed by system. See the source for a full comment about them"
msgstr ""
"Um argumento para a app de meteorologia: --display, --city, --lat, --lng "
"Eles serão geridos pelo sistema. Veja a fonte para um comentário completo "
"sobre eles"

#: ../app/ui/AddLocationPage.qml:38
msgid "Select a city"
msgstr "Selecione uma cidade"

#: ../app/ui/AddLocationPage.qml:50
msgid "City"
msgstr "Cidade"

#: ../app/ui/AddLocationPage.qml:70
msgid "Back"
msgstr "Voltar"

#: ../app/ui/AddLocationPage.qml:115
msgid "Search city"
msgstr "Procurar cidade"

#: ../app/ui/AddLocationPage.qml:288
msgid "No city found"
msgstr "Sem cidade encontrada"

#: ../app/ui/AddLocationPage.qml:301
msgid "Couldn't load weather data, please try later again!"
msgstr "Informação do tempo não carregada, por favor tente mais tarde!"

#: ../app/ui/AddLocationPage.qml:311
msgid "Location already added."
msgstr "Localização já adicionada."

#: ../app/ui/AddLocationPage.qml:314
msgid "OK"
msgstr "Confirmar"

#. TRANSLATORS: N = North, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:113
msgid "N"
msgstr "N"

#. TRANSLATORS: NE = North East, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:116
msgid "NE"
msgstr "NE"

#. TRANSLATORS: E = East, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:119
msgid "E"
msgstr "E"

#. TRANSLATORS: SE = South East, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:122
msgid "SE"
msgstr "SE"

#. TRANSLATORS: S = South, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:125
msgid "S"
msgstr "S"

#. TRANSLATORS: SW = South West, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:128
msgid "SW"
msgstr "SO"

#. TRANSLATORS: W = West, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:131
msgid "W"
msgstr "O"

#. TRANSLATORS: NW = NorthWest, wind bearing, abbreviated
#: ../app/ui/HomePage.qml:134
msgid "NW"
msgstr "NO"

#. TRANSLATORS: "%1kph" and "%1mph" is to be able to add
#. an optional space between digit+unit i.e. In English '9kph', in Catalan '9 km/h'
#: ../app/ui/LocationPane.qml:158
#, qt-format
msgid "%1kph"
msgstr "%1kph"

#: ../app/ui/LocationPane.qml:158
#, c-format, qt-format
msgid "%1mph"
msgstr "%1mph"

#: ../app/ui/LocationsPage.qml:93
msgid "Current Location"
msgstr "Localização atual"

#: ../app/ui/SettingsPage.qml:33
msgid "Settings"
msgstr "Definições"

#: ../app/ui/settings/DataProviderPage.qml:38
msgid "Data Provider"
msgstr "Fornecedor de dados"

#: ../app/ui/settings/LocationPage.qml:31
msgid "Detect current location"
msgstr "Detetar localização atual"

#: ../app/ui/settings/RefreshIntervalPage.qml:31
#: ../app/ui/settings/RefreshIntervalPage.qml:32
#: ../app/ui/settings/RefreshIntervalPage.qml:33
#: ../app/ui/settings/RefreshIntervalPage.qml:34
#: ../app/ui/settings/RefreshIntervalPage.qml:43
#, qt-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuto"
msgstr[1] "%1 minutos"

#: ../app/ui/settings/RefreshIntervalPage.qml:42
msgid "Refresh Interval"
msgstr "Intervalo de atualização"

#. TRANSLATORS: The strings are standard measurement units
#. of temperature in Celcius and are shown in the settings page.
#. Only the abbreviated form of Celcius should be used.
#: ../app/ui/settings/TempUnitsPage.qml:37
#: ../app/ui/settings/TempUnitsPage.qml:53
msgid "°C"
msgstr "°C"

#. TRANSLATORS: The strings are standard measurement units
#. of temperature in Fahrenheit and are shown in the settings page.
#. Only the abbreviated form of Fahrenheit should be used.
#: ../app/ui/settings/TempUnitsPage.qml:42
#: ../app/ui/settings/TempUnitsPage.qml:54
msgid "°F"
msgstr "°F"

#: ../app/ui/settings/TempUnitsPage.qml:52
msgid "Temperature Unit"
msgstr "Unidade de temperatura"

#: ../app/ui/settings/ThemePage.qml:31
msgid "Use Dark theme"
msgstr "Utilizar tema escuro"

#. TRANSLATORS: The strings are standard measurement units
#. of wind speed in kilometers per hour and are shown in the settings page.
#. Only the abbreviated form of kilometers per hour should be used.
#: ../app/ui/settings/WindUnitsPage.qml:36
#: ../app/ui/settings/WindUnitsPage.qml:52
msgid "kph"
msgstr "km/h"

#. TRANSLATORS: The strings are standard measurement units
#. of wind speed in miles per hour and are shown in the settings page.
#. Only the abbreviated form of miles per hour should be used.
#: ../app/ui/settings/WindUnitsPage.qml:41
#: ../app/ui/settings/WindUnitsPage.qml:53
msgid "mph"
msgstr "mph"

#: ../app/ui/settings/WindUnitsPage.qml:51
msgid "Wind Speed Unit"
msgstr "Unidade da velocidade do vento"

#: ubuntu-weather-app.desktop.in.in.h:1
msgid "Weather"
msgstr "Meteorologia"

#: ubuntu-weather-app.desktop.in.in.h:2
msgid "A weather forecast application for Ubuntu Touch"
msgstr "Uma app de meteorologia para o Ubuntu Touch"

#: ubuntu-weather-app.desktop.in.in.h:3
msgid "weather;forecast;openweathermap;sunrise;sunset;moonphase,humidity;wind;"
msgstr "meteorologia;previsão;openweathermap;pordosol;humidade;vento;"

#~ msgid "Units"
#~ msgstr "Unidades"

#~ msgid "Location"
#~ msgstr "Localização"

#~ msgid "Provider"
#~ msgstr "Fornecedor"

#~ msgid "Interval"
#~ msgstr "Intervalo"
